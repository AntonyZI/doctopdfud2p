## Issues Info

### Word
To execute current project as a service on windows, word DCOM configuration must be set as "Interactive User" due to word cannot been used by the doc conversor otherwise.

First run this at cmd:
```
mmc comexp.msc /32
```
Then:
1. Computers -> My Computer
2. DCOM Config
3. Select the Microsoft Word 97-2003 Documents -> Properties
4. Tab Identity, change from Launching User to Interactive User

## Build with pyinstaller
The "pyinstaller" module is an optional feature to keep a compacted service executable. To build the .exe file run this at an environment with the requeriments.txt installed
```
pyinstaller.exe servicePyinstaller.py --onefile --hiddenimport main --hiddenimport encodings -y
```
The executable file "servicePyinstaller.exe" can been found inside ./dist/ directory and executed like any other program or set up as a service (e.g. using [nssm.exe](https://nssm.cc/download))
