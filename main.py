from config import *

from urllib import request
from fastapi import FastAPI, HTTPException
from docx2pdf import convert
from fastapi.responses import HTMLResponse, FileResponse, RedirectResponse, JSONResponse

import os
import sys

import pythoncom

app = FastAPI()

#sys.stderr = open("mainStdErr.txt", "w")
#sys.stdout = open("mainStdOut.txt", "w")

@app.get('/convert/{docname}')
def myConvert(docname: str):
    print("docname:", docname)
    doc_filepath = os.path.join(doc_files_path, docname)
    if not os.path.exists(doc_files_path):
        raise HTTPException(status_code=500, detail="Directory '%s' not found" % doc_files_path)
    if not os.path.exists(pdf_files_path):
        raise HTTPException(status_code=500, detail="Directory '%s' not found" % pdf_files_path)
    if not os.path.exists(doc_filepath):
        raise HTTPException(status_code=404, detail="File '%s' not found" % doc_filepath)
    pdfFileName = cleanNameDocExtension(docname) + ".pdf"
    pythoncom.CoInitialize()
    convert(doc_filepath, os.path.join(pdf_files_path, pdfFileName))
    return JSONResponse(content=pdfFileName, status_code=200)

def cleanNameDocExtension(file_name):
    inverseName = file_name[::-1]
    fileExten = "cod." # Extension de archivo invertido
    cleanFName = inverseName[: inverseName.find(fileExten) + len(fileExten) - 1: -1]
    return cleanFName
